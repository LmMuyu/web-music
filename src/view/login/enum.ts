export enum STATUS {
  EXPIRED = 800,
  WAIT = 801,
  TODECONFIRMED = 802,
  RESLUT = 803,
}

export enum COMP {
  OTHERLOGIN = "otherLogin",
  QRLOGIN = "qrlogin",
  LOGINWITHPHONE = "loginwithphone",
}
