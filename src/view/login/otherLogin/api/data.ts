export interface thridparty {
  icon: string;
  text: string;
  color: string;
  size: string;
}

export const thirdPartyLogin: thridparty[] = [
  {
    icon: "iconweixin",
    text: "微信",
    color: "#2ed573",
    size: "28px",
  },
  {
    icon: "iconv3-fuben_QQ",
    text: "QQ",
    color: "#00a8ff",
    size: "28px",
  },
];

export const footerDeal = [
  {
    text: "服务条款",
  },
  {
    text: "隐私政策",
  },
  {
    text: "儿童隐私政策",
  },
];
