const userModule = [
  {
    icon: "iconwode",
    moduleText: "个人中心",
  },
  {
    icon: "iconxinxi",
    moduleText: "个人信息",
  },
  {
    icon: "icondengji",
    moduleText: "个人等级",
  },
  {
    icon: "iconshezhi",
    moduleText: "个人设置",
  },
  {
    icon: "icontuichu",
    moduleText: "退出",
  },
];

export { userModule };
