import { ElCol, ElRow } from "element-plus";
import addCompName from "./utils/compName";

const comps = [ElCol, ElRow];

addCompName(comps);

// app.component(ElCol.name)
