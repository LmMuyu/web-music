export const contentTags = [
  {
    _id: "1000",
    tagname: "推荐",
    comname: "Discover",
    comchildren: "DiscoverBar",
  },
  {
    _id: "1001",
    tagname: "排行榜",
    comname: "Toplist",
    comchildren: "TopListAsideTag",
  },
  {
    _id: "1002",
    tagname: "歌单",
    comname: "playlist",
  },
  {
    _id: "1003",
    tagname: "主播电台",
    comname: "djradio",
  },
  {
    _id: "1004",
    tagname: "歌手",
    comname: "artist",
  },
  {
    _id: "1005",
    tagname: "新碟上映",
    comname: "album",
  },
];
